﻿const debug = require('debug');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');


const auth = require('./routes/auth');
const api = require('./routes/api');
const developer = require('./routes/developer');
const userDB = require('./models/users');
const cors = require('cors');
const TG = require('./modules/tgbot');

//Создание апликейшна
    const app = express();

// view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'pug');

//Lab7 
//---------------------------------------------------------------------------
    // uncomment after placing your favicon in /public
    //app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(express.static(path.join(__dirname, 'dist')));
    app.use(busboyBodyParser({}));
    app.use(cookieParser());
    app.use(session({
        secret: "Some_secret^string",
        resave: false,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
passport.use(new LocalStrategy(
    function (username, password, done) {
        userDB.getUserByLogin(username)
            .then(usr => {
                return usr && usr.password != userDB.hash(password) ? done(null, false) : done(null, usr)
            }
            )
            .catch(err => done(err, null));
    }
));
//
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;


passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'your_jwt_secret'
},
    function (jwtPayload, cb) {
        //find the user in db if needed.
        //This functionality may be omitted if you store everything you'll need in JWT payload.
        console.log('JWTStrategy jwtPayload' + jwtPayload);
        return userDB.getUserByID(jwtPayload.id)
            .then(user => cb(null, user))
            .catch(err => cb(err));
    }
));

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });
    passport.deserializeUser(function (id, done) {
        userDB.getUserByID(id)
            .then(usr => done(null, usr))
            .catch(err => done(err, null));
    });
    


//---------------------------------------------------------------------------

//Руты
    app.use('/auth', auth);
    app.use('/api/v1', api);
app.use('*', function (req, res) {
    res.sendFile(process.cwd() + "/dist/index.html");
})
// error handlers
// catch 404 and forward to error handler
    app.use(function (req, res, next) {
        const err = new Error('Страница не найдена!');
        err.status = 404;
        next(err);
    });



// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            me: req.user, 
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);


//Монгусь
const mongoose = require('mongoose');
const url = process.env.DATABASE_URL || 'mongodb://localhost:27017/Lab5';
const conOptions = { useNewUrlParser: true };
mongoose.connect(url, conOptions)
    .then(() => console.log(`Database connected: ${url}`))
    .then(() => {
        const server = app.listen(app.get('port'), () => {
            debug('Express server listening on port ' + server.address().port);
        });
    })
    .catch(err => console.log(`Status error: ${err}`));