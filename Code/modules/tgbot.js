﻿
const TelegramBotApi = require('telegram-bot-api');
const User = require('../models/users');
const telegramBotApi = new TelegramBotApi({
    token: process.env.tgb_token,
    updates: {
        enabled: true  // do message pull
    }
});



telegramBotApi.on('message', onMessage);
function onMessage(message) {
    processRequest(message)
        .catch(err => telegramBotApi.sendMessage({
            chat_id: message.chat.id,
            text: `Something went wrong. Try again later. Error: ${err.toString()}`,
        }));
}

async function processRequest(message) {
    const chatId = message.chat.id;
    const username = message.from.username;
    if (message.text === '/start') {
        const usr = await User.registerTgUser(username, chatId);
        console.log(`User ${username} registered on chat ${chatId}`);
        return telegramBotApi.sendMessage({
            chat_id: chatId,
            text: usr ? "Спасибо, что присоединились к Вонголе!" : `Похоже, что пользователя с именем ${username} не найден на сайте!`,
        });
    }
    console.log(message.from.username, message.text);
    return telegramBotApi.sendMessage({
        chat_id: chatId,
        text: message.text,
    });
}

module.exports = {
    async sendNotification(text, userId) {
        console.log('text ' + text);
        console.log('userId ' + userId);
        const user = await User.getUserByID(userId);
        if (user.TG_chatId != '')
            await telegramBotApi.sendMessage({
                chat_id: user.TG_chatId,
                text: text,
                parse_mode: 'Markdown'
            });
    }
};