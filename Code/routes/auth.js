﻿const express = require('express');
const router = express.Router();
const User = require("../models/users");
const passport = require('passport');
const cloud = require("./additional/cloud")
const jwt = require('jsonwebtoken');
router.post("/login", function (req, res) {

    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err != null)
            return res.json({
                message: `There is an error: ${err}`,
                user: user,
                if: err != null
            });
        if ( !user) {
            return res.json({
                message: `No user: ${JSON.stringify(info)}`,
                user: user
            });
        }
        req.login(user, { session: false }, (err) => {
            if (err) { return res.send(err); }
            // generate a signed json web token with the contents of user object
            const token = jwt.sign(user.toObject(), 'your_jwt_secret');
            return res.json({ user, token });
        });
    })(req, res);
});

router.get('/logout', (req, res) => {
    req.logout();
    res.sendStatus(200);
});
router.get('/auth', (req, res) => {
    res.render('userForm', { me: req.user, title: "Новый Пользователь", err: req.query.err });
});
router.post('/register', (req, res, next) => {
    console.log('REGISTERED');
    console.log(req);
    if (req.body.pass != req.body.repass)
        res.json({ message: "Пароли не совпали!" })
    else {
        return cloud.handle_file_upload_promised(Buffer.from(new Uint8Array(req.files.Ava.data)))
            .then(result => User.insert(new User.User(
                req.body.login,
                User.hash(req.body.pass),
                req.body.fullname,
                result.url,
                req.body.bio)))
            .then(usr => { res.json(usr); })
            .catch(err => res.json(err));
    }
});

module.exports = router;
