﻿                //АЛО блин симба! Ты дурак! Способ для изменения картинки прост!
                //Если ты сделаешь форму, в которой не будешь прописывать велью и сделаешь ВСЕ поля не обязательными для ввода,
                //то при редактировании сущности можно счтать, что пользователь хочет оставить  невведенные поля старыми!
const express = require('express');
const router = express.Router();
const User = require("../models/users");
const Article = require("../models/articles");
const Comment = require("../models/comment");
const pagination = require("./additional/Pagination");
const cloud = require("./additional/cloud");
const passport = require('passport');
const jwt = require('jsonwebtoken');
function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (bearerHeader != undefined) {
        req.token = bearerHeader.split(' ')[1];
        next();
    }
    else {
        res.sendStatus(401);
    }
}

function userToResult(user, currentPage) {
    const displayed = pagination(user.articles, currentPage);
    for (let art in displayed.resultOfPagination) {
        art.comments = art.comments ? art.comments.length : 0;
    }
    user.articles = displayed.resultOfPagination;
    user.password = undefined;
    return {
        currentPageOfArticles: displayed.page,
        maxPageOfArticles: displayed.maxPage,
        user: user
    };
}
function articleToResult(article, currentPage) {
    const displayed = pagination(article.comments, currentPage);
    article.comments = displayed.resultOfPagination;
    article.author.articles = undefined;
    article.author.password = undefined;
    return{
        currentPageOfArticles: displayed.page,
        maxPageOfArticles: displayed.maxPage,
        article: article
    };
}

router.get('/me',
    verifyToken, 
    function (req, res) {
        res.json(userToResult(jwt.verify(req.token, 'your_jwt_secret')));
    });
router.get('/users',
    verifyToken,
    function (req, res) {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        if (!ReqUser || ReqUser.role != 1) {
            res.json({ errorCode: 403, errorMassage: !ReqUser ? "Гости не могут делать этот запрос" : "Только Администраторы могут делать этот запрос" });
            return;
        }
        const currentPage = !req.query.page || isNaN(Number(req.query.page)) || Number(req.query.page) <= 0 ? 1 : Number(req.query.page);
        User.getAll()
            .then(users => {
                const displayed = pagination(users, currentPage);
                //Вместо массива айдишников статей оставим только количество их
                //Так же уберем информацию о паролях
                for (let user of displayed.resultOfPagination) {
                    //user.NumberOfArticles = user.articles.length;
                    user.articles = undefined;
                    user.password = undefined;
                }
                res.json({
                    currentPage: displayed.page, 
                    maxPage: displayed.maxPage,
                    users: displayed.resultOfPagination
                });
            })
            .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));;
});
router.put('/users',
    verifyToken,
     async (req, res) => {
    //Ожидается, что в теле придет вся информация о новых данных пользоваеля
         const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
    const currentPage = !req.query.page || Number(req.query.page) <= 0 ? 1 : Number(req.query.page);
    if (!req.body.id) res.json({ errorCode: 400, errorMassage: 'Bad request' });
    else {
        try {
            let usr = await User.getUserByID(req.body.id);
            if (req.body.role) {
                if (!ReqUser || ReqUser.role != 1) {
                    res.json({ errorCode: 400, errorMassage: "Недостаточно привелегий" });
                    return;
                }
                usr.role =  req.body.role;
            }
            else {
                if (!ReqUser || (!ReqUser._id == usr._id)) {
                    res.json({ errorCode: 400, errorMassage: "Не верный пароль" });
                    return;
                }
                usr.password = req.body.newPassword ? User.hash(req.body.newPassword) : usr.password;
                usr.fullname = req.body.fullname ? req.body.fullname : usr.fullname;
                usr.bio = req.body.bio ? req.body.bio : usr.bio;
                usr.TG_username = req.body.TG_username ? String(req.body.TG_username) : usr.TG_username;
                usr.TG_chatId = req.body.TG_chatId ? req.body.TG_chatId : usr.TG_chatId;
                if (req.files && req.files.Ava) {
                    await cloud.delete_file_promised(usr.avaUrl.substring(usr.avaUrl.lastIndexOf('/') + 1));
                    const result = await cloud.handle_file_upload_promised(Buffer.from(new Uint8Array(req.files.Ava.data)));
                    usr.avaUrl = result.url;
                }
            }
            const updatedUser = await User.update(usr);
            res.json(userToResult(updatedUser, currentPage, ReqUser));
        }
        catch (err) {
            res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack })
        }
    }
});

router.get('/users/:id',
    function (req, res) {
        const currentPage = !req.query.page || isNaN(Number(req.query.page)) || Number(req.query.page) <= 0 ? 1 : Number(req.query.page);
    User.getUserByID(req.params.id)
        .then(user => res.json(userToResult(user, currentPage)))
        .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
});

//--------------------------------------------------------------------------------------------------------

router.get('/articles',
    function (req, res) {
        const currentPage = !req.query.page || isNaN(Number(req.query.page)) || Number(req.query.page) <= 0 ? 1 : Number(req.query.page);
        Article.getArticles(req.query.sarch_criteria)
            .then(arts => {
            const displayed = pagination(arts, currentPage);
            for (let art of displayed.resultOfPagination) {
                art.comments = undefined;
                art.author.articles = undefined;
                art.author.password = undefined;
            } 
            res.json({
                currentPage: displayed.page,
                maxPage: displayed.maxPage,
                articles: displayed.resultOfPagination
            });
        })
        .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
});
router.delete('/articles/:id',
    verifyToken,
    function (req, res) {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        const id = req.params.id;
        Article.getArticleByID(id)
            .then(art => {
                return !ReqUser || (!ReqUser._id == art.author._id && ReqUser.role != 1) ?
                    Promise.reject(`Ошибка доступа. Вносить изменения в приватные объекты может только владелец или администратор!`) :
                    cloud.delete_file_promised(art.avaUrl.substring(art.avaUrl.lastIndexOf('/') + 1));
            })
            .then(result => Article.remove(id))
            .then(result => res.json({ errorCode: 200, errorMassage: "Ok" }))
            .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
});
router.put('/articles',
    verifyToken,
    async (req, res) => {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        const currentPage = !req.query.page || isNaN(Number(req.query.page)) || Number(req.query.page) <= 0 ? 1 : Number(req.query.page);
        if (!req.body.id)  res.json({ errorCode: 400, errorMassage: 'Bad request' });
        else {
            try {
                let art = await Article.getArticleByID(req.body.id);
                if (!ReqUser || !ReqUser._id == art.author._id) {
                    res.json({ errorCode: 400, errorMassage: "Недостаточно привелегий" });
                    return;
                }
                art.shortReview = req.body.shortReview ? req.body.shortReview : art.shortReview;
                art.title = req.body.title ? req.body.title : art.title;
                art.likes = req.body.likes ? Number(req.body.likes) : art.likes;
                art.text = req.body.text ? Article.textTranslation(req.body.text) : art.text;
                if (req.files.Ava) {
                    await cloud.delete_file_promised(art.avaUrl.substring(art.avaUrl.lastIndexOf('/') + 1));
                    const result = await cloud.handle_file_upload_promised(Buffer.from(new Uint8Array(req.files.Ava.data)));
                    art.avaUrl = result.url;
                }
                const updatedArticle = await Article.update(art);
                res.json(articleToResult(updatedArticle, currentPage, ReqUser));
            }
            catch (err) {
                res.json({ errorCode: 400, errorMassage: err.message, errorStack: err.stack });
            }
        }
});
router.post('/articles',
    verifyToken,
    async (req, res) => {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        if (!ReqUser) {
            res.json({ errorCode: 403, errorMassage: "Создавать объекты могут только авторизированные пользователи" });
        }   
        else cloud.handle_file_upload_promised(Buffer.from(new Uint8Array(req.files.Ava.data)))
            .then(result => Article.insert(new Article.Article(
                req.body.title,
                ReqUser._id,
                result.url,
                req.body.shortReview,
                req.body.text)))
            .then(([art, usr]) => res.json(art) )
            .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
});
router.get('/articles/:id',
    function (req, res) {
        const currentPage = !req.query.page || isNaN(Number(req.query.page)) || Number(req.query.page) <= 0 ? 1 : Number(req.query.page);
    const id = req.params.id;
    Article.getArticleByID(id)
        .populate("author").populate({
            path: "comments",
            model: "Comment",
            populate: {
                path: "author",
                model: "User",
            }
        })
        .exec()
        .then(article => {
            if (!article) {
                res.json({ errorCode: 404, errorMassage: 'Статья не найденa' });
            }
            else {
                res.json(articleToResult(article, currentPage));
            }
        }
    ).catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
    });
//--------------------------------------------------------------------------------------------------------------
router.delete('/comments/:id/:artId',
    verifyToken,
    (req, res) => {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        const id = req.params.id;
        const artId = req.params.artId;
        Comment.getCommentByID(id)
            .then(com => {
                if (!ReqUser || (!ReqUser._id == com.author._id && ReqUser.role != 1)) {
                    const err = new Error(`Ошибка доступа. Вносить изменения в приватные объекты может только владелец или администратор!`);
                    err.status = 403;
                    return Promise.reject(err);
                }
                return Promise.resolve(Comment.remove(id, artId));
            })
            .then(x => res.json({ errorCode: 200, errorMassage: 'Ok' }))
            .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
    });
router.put('/comments',
    verifyToken,
    async (req, res) => {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        if (!req.body.id) res.json({ errorCode: 400, errorMassage: 'Bad request' });
        else {
            try {
                let comment = await Comment.getCommentByID(req.body.id);
                if (!ReqUser || !ReqUser._id == comment.author._id) {
                    res.json({ errorCode: 400, errorMassage: "Недостаточно привелегий" });
                    return;
                }
                comment.text = req.body.text ? req.body.text : comment.text;
                comment.likes = req.body.likes ? Number(req.body.likes) : comment.likes;
                const updatedComment = await Comment.update(comment);
                updatedComment.author.password = undefined;
                updatedComment.author.articles = undefined;
                res.json(updatedComment);
            }
            catch (err) {
                res.json({ errorCode: 400, errorMassage: err.message, errorStack: err.stack });
            }
        }
    });
router.post('/comments',
    verifyToken,
    async (req, res) => {
        const ReqUser = jwt.verify(req.token, 'your_jwt_secret');
        if (!ReqUser) {
            res.json({ errorCode: 403, errorMassage: "`Ошибка доступа. Вносить изменения в приватные объекты может только владелец или администратор!`" });
        }
        else Comment.insert(new Comment.Comment(ReqUser._id, req.body.text), req.body.artId)
            .then(([comment, article]) => res.json(comment))
            .catch(err => res.json({ errorCode: 500, errorMassage: err.message, errorStack: err.stack }));
    });


module.exports = router;
