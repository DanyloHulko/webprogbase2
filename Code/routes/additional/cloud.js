﻿const cloudinary = require('cloudinary');
const util = require('util');
cloudinary.config({
    cloud_name: process.env.cloud_name ,
    api_key: process.env.api_key ,
    api_secret: process.env.api_secret 
});
const handle_file_upload_promised = util.promisify(handleFileUpload);
const delete_file_promised = util.promisify(deleteFile);
function handleFileUpload(fileBuffer, callback) {
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
        function (error, result) {
            if (error) callback(error)
            else callback(null, result);
        })
        .end(fileBuffer);
}
function deleteFile(id, callback) {
    cloudinary.uploader.destroy(id, function (info) {
        let result, err;
        err = info.error;
        result = info.result;
        if (err) callback(err)
        else if (result != "ok") callback(new Error("No file to delete"))
        else callback(null, result);
    }, { resource_type: 'raw' });
}

module.exports = {
    handle_file_upload_promised: handle_file_upload_promised,
    delete_file_promised: delete_file_promised
}