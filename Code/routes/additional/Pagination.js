﻿const PAGINATION = 5;

function pagination(array, page) {
    const delta = array.length % PAGINATION == 0 ? 0 : 1;
    const maxPage = parseInt(array.length / PAGINATION) + delta;
    if (page > maxPage) page = maxPage;
    let bound = page * PAGINATION;
    if (bound > array.length) bound = array.length;
    return {
        resultOfPagination: array.slice(PAGINATION * (page - 1), bound),
        page: page,
        maxPage: maxPage
    };
}

module.exports = pagination;
