﻿const express = require('express');
const router = express.Router();

router.get('/', function (req, res) {
    res.render('developer_v1', { me: req.user, title: 'Документация для REST v1' });
});

module.exports = router;