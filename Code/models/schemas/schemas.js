﻿const mongoose = require('mongoose');
//const CommentDB = require("./comment");
let Schema = mongoose.Schema;
const ArticleSchema = new mongoose.Schema({
    title: { type: String, require: true },
    author: { type: mongoose.mongo.ObjectId, ref: "User", required: true },
    registeredAt: { type: Date, default: Date.now },
    avaUrl: { type: String, require: true },
    text: [{
        p: { type: String },
        imgUrl: { type: String }
    }],
    shortReview: { type: String, required: true },
    condition: { type: Number, default: 0 },
    likes: { type: Number, default: 0 },
    comments: [{ type: Schema.Types.ObjectId, ref: "Comment" }]
});
const UserSchema = new mongoose.Schema({
    login: { type: String, require: true, unique: true },
    password: { type: String, require: true },
    TG_username: { type: String },
    TG_chatId: { type: String },
    role: { type: Number, default: 0 },
    fullname: { type: String, require: true },
    registeredAt: { type: Date, default: Date.now },
    avaUrl: { type: String, require: true },
    isDisabled: { type: Boolean, default: false },
    bio: { type: String, require: true },
    articles: [{
        type: Schema.Types.ObjectId, ref: "Article"
    }]
});
const CommentSchema = new mongoose.Schema({
    author: { type: mongoose.mongo.ObjectId, ref: "User", required: true },
    registeredAt: { type: Date, default: Date.now },
    text: { type: String, required: true },
    likes: { type: Number, default: 0 }
});


module.exports = {
    ArticleSchema: ArticleSchema,
    UserSchema: UserSchema,
    CommentSchema: CommentSchema
}