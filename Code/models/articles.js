﻿const TMP_NAME_OF_FILE = "TMP_NAME_OF_FILE";
const schemas = require("./schemas/schemas");
const mongoose = require('mongoose');
const UserDB = require("./users");
const ArticleModel = mongoose.model('Article', schemas.ArticleSchema);
const CommentModel = mongoose.model('Comment', schemas.CommentSchema);
const TG = require('../modules/tgbot');
function translate(input) {
    let output;
    if (input == undefined) return input;
    if (input[0].p) {
        output = "";
        for (let x of input) {
            output += x.p + '\n';
        }
    }
    else {
        input = input.split("\n");
        output = [];
        for (let x of input) {
            output.push({ p: x });
        }
    }
    return output;
}
module.exports = {
    Article: function (title, author, avaUrl, shortReview, text, usr) {
        this.text = translate(text);
        this.title = title;
        this.avaUrl = avaUrl;
        this.shortReview = shortReview;
        this.author = author;
    },
    getArticleByID: function (id) {
        return ArticleModel.findOne({ _id: id });
    },
    getArticles: function (criteria) {
        return ArticleModel.find().sort({ created: -1 })
            .populate("author")
            .then(art => {
                art = art.reverse();
                if (criteria) {
                    let result = [];
                    //Надо сдлеать поиск по контенту
                    for (let x of art) {
                        if (x.title.toUpperCase().indexOf(criteria.toUpperCase()) != -1) result.push(x);
                    }
                    return result;
                }
                else {
                    return art;
                }
            }); 
    },
    insert: function (article) {
        const mod = new ArticleModel(article);
        return mod.save()
            .then(savedArt => {
                savedArt.avaUrl = savedArt.avaUrl.replace(TMP_NAME_OF_FILE, savedArt._id.toString());
                return Promise.all([
                    ArticleModel.findOneAndUpdate({ _id: savedArt._id }, savedArt, { new: true }),
                    UserDB.addArticle(savedArt.author, savedArt._id)
                ]);
            }).then(arr => {
                TG.sendNotification(`Вы добавили новую статью:\n[${arr[0].title}](https://vongola-blog.herokuapp.com/articles/${arr[0]._id})`, arr[0].author)
                return arr;
            });
    },
    update: function (article) {
        return ArticleModel.findOneAndUpdate({ _id: article._id }, article, { new: true })
            .then(art => {
                TG.sendNotification(`Вы обновили свою статью :\n[${art.title}](https://vongola-blog.herokuapp.com/articles/${art._id})`, art.author)
                return art;
            });
    },
    remove: function (id) {
        return ArticleModel.findOne({ _id: id })
            .populate("author").populate({
                path: "comments",
                model: "Comment",
                populate: {
                    path: "author",
                    model: "User",
                }
            })
            .exec()
            .then(art => {
                let ar = [];
                for (let com of art.comments) {
                    ar.push(CommentModel.deleteOne({ _id: com._id }));
                }
                ar.push(ArticleModel.deleteOne({ _id: id }));
                ar.push(UserDB.removeArticle(art.author, id));
                TG.sendNotification(`Вы удалили свою статью:\n${art.title}`, art.author)
                return Promise.all(ar);
            })
        
    },
    addComment: function (artId, comId) {
        return ArticleModel.findOne({ _id: artId })
            .then(art => {
                art.comments.push(comId);
                return ArticleModel.findOneAndUpdate({ _id: art._id }, art, { new: true });
            });
    },
    removeComment: function (artId, comId) {
        return ArticleModel.findOne({ _id: artId })
            .then(art => {
                art.comments.splice(art.comments.indexOf(comId), 1);
                return ArticleModel.findOneAndUpdate({ _id: art._id }, art, { new: true });
            })
            .catch(err => { console.log(err); });
    },
    TMP_NAME_OF_FILE: TMP_NAME_OF_FILE,
    textTranslation: translate
};