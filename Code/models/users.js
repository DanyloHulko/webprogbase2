﻿const schemas = require("./schemas/schemas");
const mongoose = require('mongoose');
const UserModel = mongoose.model('User', schemas.UserSchema);
const md5 = require('js-md5');
const saltExternal = 'P9s2oO293R3dwT389EeR';
const saltInterior = 'P9ui9A909pklkB';


module.exports = {
    User: function (login, password, fullname, avaUrl, bio) {
        this.login = login;
        this.password = password;
        this.fullname = fullname;
        this.avaUrl = avaUrl;
        this.bio = bio;
    },
    getUserByID: function (id) {
        return UserModel.findOne({ _id: id }).populate("articles"); 
    }, 
    getUserByLogin: function (login) {
        return UserModel.findOne({ login: login });
    }, 
    getAll: function () {
        return UserModel.find().populate("articles"); 
    },
    insert: function (user) {
        const mod = new UserModel(user);
        return mod.save();
    },
    update: function (updatedUsr) {
        return UserModel.findOneAndUpdate({ _id: updatedUsr._id }, updatedUsr, { new: true });
    },
    delete: function (id) {
        return UserModel.deleteOne({ _id: id });
    },
    addArticle: function (usrId, artId) {
        return UserModel.findOne({ _id: usrId })
            .then(user => {
                user.articles.push(artId);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
            });
    },
    removeArticle: function (usrId, artId) {
        return UserModel.findOne({ _id: usrId })
            .then(user => {
                user.articles.splice(user.articles.indexOf(artId), 1);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
            });
    },
    registerTgUser: function (TG_username, TG_chatId) {
        return UserModel.findOne({ TG_username: TG_username })
            .then(usr => {
                if (!usr) return null;
                usr.TG_chatId = TG_chatId;
                return UserModel.findOneAndUpdate({ _id: usr._id }, usr, { new: true })
            })
    },
    hash: function (str) {
        return saltExternal + md5(saltInterior + str);
    }
};
