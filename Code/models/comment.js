﻿const schemas = require("./schemas/schemas");
const mongoose = require('mongoose');
const CommentModel = mongoose.model('Comment', schemas.CommentSchema);
const ArticelDB = require("./articles");
const TG = require('../modules/tgbot');
module.exports = {
    Comment: function (author, text) {
        console.log('comment create report');
        console.log('author: ' + author);
        console.log('text' + text);
        this.author = author;
        this.text = text;
    },
    getCommentByID: function (id) {
        return CommentModel.findOne({ _id: id }).populate("author");
    },
    insert : function (comment, artId) {
        const mod = new CommentModel(comment);
        return mod.save()
            .then(saved => {
                return Promise.all([
                    saved,
                    ArticelDB.addComment(artId, saved._id)
                ]);
            })
            .then(arr => {
                if (!arr[1].author.equals(arr[0].author))
                    TG.sendNotification(`К вашей статье добавили новый комментарий:\n[${arr[1].title}](https://vongola-blog.herokuapp.com/articles/${arr[1]._id})`, arr[1].author)
                TG.sendNotification(`Вы добавили новый комментарий к статье:\n[${arr[1].title}](https://vongola-blog.herokuapp.com/articles/${arr[1]._id})`, arr[0].author)
                return arr;
            });
    },
    update: function (comment) {
        return CommentModel.findOneAndUpdate({ _id: comment._id }, comment, { new: true }).populate("author");
    },
    remove: function (comId, artId) {
        return CommentModel.findOne({ _id: comId })
            .then(com => {
                return Promise.all([
                    CommentModel.deleteOne({ _id: comId }),
                    ArticelDB.removeComment(artId, comId),
                    com.author
                ])
            })
        .then(arr => {
            console.log(`Вы удалили свой комментарий к статье:\n[${arr[1].title}](https://vongola-blog.herokuapp.com/articles/${arr[1]._id})`, arr[2]);
            TG.sendNotification(`Вы удалили свой комментарий к статье:\n[${arr[1].title}](https://vongola-blog.herokuapp.com/articles/${arr[1]._id})`, arr[2])
            return Promise.all([
                arr[0], arr[1]
            ])
        })
        
    }

};