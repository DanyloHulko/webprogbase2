﻿function assention(btn) {
    const someDataObject = { id: btn.getAttribute("usrId"), role: 1 };
    const fetchOptions = {
        method: "PUT", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify(someDataObject), // body data type must match "Content-Type"
    };
    fetch('/api/v1/users', fetchOptions)  // POST request
        .then(function (response) {
            console.log(response.status);
            console.log(response.headers);
            return response.json();
        })
        .then(function (myJson) {
            console.log(myJson);
            document.getElementById('notAdmin').innerHTML = '';
            document.getElementById('usrFullname').innerHTML = document.getElementById('usrFullname').innerHTML + " (Администратор)";
        });
}
function TG_set(btn) {
    const someDataObject = { id: btn.getAttribute("usrId"), TG_username: document.getElementById('UsernameTG').value };
    const fetchOptions = {
        method: "PUT", // *GET, POST, PUT, DELETE, etc.  
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify(someDataObject), // body data type must match "Content-Type"
    };
    fetch('/api/v1/users', fetchOptions)  
        .then(function (response) {
            console.log(response.status);
            console.log(response.headers);
            return response.json();
        })
        .then(function (myJson) {
            console.log(myJson);
            document.getElementById('pTG_USRNAME').innerHTML = document.getElementById('UsernameTG').value;
        });
}


function action(object, dist) {
    fetch('/api/v1/users/' + object.id + '?page=' + String(object.currentPage + dist))
        .then(x => x.json())
        .then((itemsData) => {
            object.currentPage = itemsData.currentPageOfArticles;
            object.maxPage = itemsData.maxPageOfArticles;
            object.articles = itemsData.user.articles;
        })
        .catch(err => console.log(err));
}


 new Vue({
     el: '#ArticlesList',
     data: {
        id: '',
        currentPage: 1,
        maxPage: 1,
        articles: null,
    },
    methods: {
        next: function () {
            action(this, 1)
        },
        prev: function () {
            action(this, -1)
        }
    },
     mounted: function () {
        this.id = document.getElementById('userId').value;
        action(this, 0)
    }
});