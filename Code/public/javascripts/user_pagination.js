﻿function action(object, dist) {
    fetch('/api/v1/users?page=' + String(object.currentPage + dist))
        .then(x => x.json())
        .then((itemsData) => {
            object.currentPage = itemsData.currentPage;
            object.maxPage = itemsData.maxPage;
            object.users = itemsData.users;
        })
        .catch(err => console.log(err));
}


const comm = new Vue({
    el: '#UserTable',
    data: {
        currentPage: 1,
        maxPage: 1,
        users: null,
    },
    methods: {
        next: function () {
            action(this, 1)
        },
        prev: function () {
            action(this, -1)
        }
    },
    mounted: function () {
        action(this, 0)
    }
});