﻿var app = new Vue({
    el: '#dd',
    data: {
        isDisplayed: true,
        input: '',
        articles_dd: []
        
    },
    methods: {
        searchFilter: function () {
            if (this.input == '') this.isDisplayed = true;
            else
                fetch('/api/v1/articles?sarch_criteria=' + this.input).then(x => x.json())
                    .then((itemsData) => {
                        this.isDisplayed = false;
                        this.articles_dd = itemsData.articles;
                    })
                    .catch(err => console.log(err));
        },
        clearListOfSearch: function () {
            if (event.relatedTarget == null || !event.relatedTarget.href)
                this.isDisplayed = true;
        }
    }
});