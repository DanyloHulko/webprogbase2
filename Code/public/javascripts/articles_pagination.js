﻿


function act(movement) {
    let page = Number(document.getElementById("page").value) + Number(movement);
    const sarch_criteria = document.getElementById("sarch_criteria").value.replace(' ', '+');
    let maxpage = 0;
    Promise.all([
        fetch("/templates/article_list.mst").then(x => x.text()),
        fetch('/api/v1/articles?sarch_criteria=' + sarch_criteria + '&page=' + page).then(x => x.json()),
    ])
        .then(([templateStr, itemsData]) => {
            //itemsData.tableTitle = document.getElementById("tableTitle").textContent;
            const renderedHtmlStr = Mustache.render(templateStr, itemsData);
            if (page <= 0) page = 1;
            if (page > itemsData.maxPage) page = itemsData.maxPage;
            maxpage = itemsData.maxPage;
            return renderedHtmlStr;
        })
        .then(htmlStr => {
            document.getElementById("page").value = page;
            document.getElementById('ArticlesList').innerHTML = htmlStr;
            //class="page-item disabled"
            document.getElementById('PREV').style.visibility = maxpage != 0 && page != 1 ? 'visible' : 'hidden';
            document.getElementById('NEXT').style.visibility = maxpage != 0 && page != maxpage ? 'visible' : 'hidden';
        })
        .catch(err => console.log(err));
}

function action(object, dist) {
    fetch('/api/v1/articles?sarch_criteria=' + object.sarch_criteria + '&page=' + String(object.currentPage + dist))
        .then(x => x.json())
        .then((itemsData) => {
            object.currentPage = itemsData.currentPage;
            object.maxPage = itemsData.maxPage;
            object.articles = itemsData.articles;
        })
        .catch(err => console.log(err));
}


new Vue({
    el: '#ArticlesList',
    data: {
        sarch_criteria: '',
        currentPage: 1,
        maxPage: 1,
        articles: null,
    },
    methods: {
        next: function () {
            action(this, 1)
        },
        prev: function () {
            action(this, -1)
        }
    },
    mounted: function () {
        this.sarch_criteria = document.getElementById('sarch_criteria').value;
        action(this, 0)
    }
});