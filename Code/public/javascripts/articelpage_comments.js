﻿function action(object, dist) {
    console.log('path', '/api/v1/articles/' + String(object.id) + '?page=' + String(object.currentPage + dist) )
    Promise.all([
        fetch("/api/v1/me?localOnly=true").then(x => x.json()),
        fetch('/api/v1/articles/' + object.id + '?page=' + String(object.currentPage + dist)).then(x => x.json()),
    ])
        .then(([me, itemsData]) => {
            console.log('items', itemsData);
            object.currentPage = itemsData.currentPageOfArticles;
            object.maxPage = itemsData.maxPageOfArticles;
            object.article = itemsData.article;
            object.comments = [];
            for (let i in itemsData.article.comments) {
                console.log('ind', 'a' + i)
                object.comments.push({
                    btn: me != undefined && (me.user.role == 1 || itemsData.article.comments[i].author._id == me.user._id),
                    com: itemsData.article.comments[i],
                    ind: 'a' + i
                });
            }
        })
        .catch(err => console.log(err));
}


const comm = new Vue({
    el: '#CommentsList',
    data: {
        id: "",
        currentPage: 1,
        maxPage: 1,
        article: null,
        comments: null,
        toRemove: ''
    },
    methods: {
        next: function () {
            action(this, 1)
        },
        prev: function () {
            action(this, -1)
        },
        removeComment: function (id) {
            const fetchOptions = {
                method: "DELETE", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    // "Content-Type": "application/x-www-form-urlencoded",
                },
                redirect: "follow", // manual, *follow, error
                referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify({ id: id, artId: this.id }), // body data type must match "Content-Type"
            };
            console.log('delete ida', this.id)
            console.log('delete idc', id)
            fetch('/api/v1/comments', fetchOptions) 
                .then((response) => {
                    console.log(response.status);
                    console.log(response.headers);
                    return response.json();
                })
                .then((myJson) => {
                    console.log(myJson);
                    console.log('delete ida', this)
                    action(this, 0)
                });
        }
    },
    mounted: function () {
        this.id = document.getElementById("article_id").value;
        action(this, 0)
    }
});